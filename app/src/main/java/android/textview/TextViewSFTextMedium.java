package android.textview;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.lexcorp.demoapp.util.Constants;

public class TextViewSFTextMedium extends TextView {

    public TextViewSFTextMedium(Context context) {
        super(context);
        applyCustomFont();
    }

    public TextViewSFTextMedium(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public TextViewSFTextMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFTEXT_MEDIUM)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFTEXT_MEDIUM);
            setTypeface(tf);
        }
    }
}
