package android.edittext;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;

import com.lexcorp.demoapp.util.Constants;

public class EditTextSFTextSemiBold extends EditText {

    public EditTextSFTextSemiBold(Context context) {
        super(context);
        applyCustomFont();
    }

    public EditTextSFTextSemiBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public EditTextSFTextSemiBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFTEXT_SEMIBOLD)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFTEXT_SEMIBOLD);
            setTypeface(tf);
        }
    }
}
