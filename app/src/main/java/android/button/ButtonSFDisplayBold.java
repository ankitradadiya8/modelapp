package android.button;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.lexcorp.demoapp.util.Constants;

public class ButtonSFDisplayBold extends Button {

    public ButtonSFDisplayBold(Context context) {
        super(context);
        applyCustomFont();
    }

    public ButtonSFDisplayBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public ButtonSFDisplayBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFDISPLAY_BOLD)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFDISPLAY_BOLD);
            setTypeface(tf);
        }
    }
}
