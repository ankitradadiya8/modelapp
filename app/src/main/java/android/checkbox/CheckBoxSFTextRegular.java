package android.checkbox;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.lexcorp.demoapp.util.Constants;

public class CheckBoxSFTextRegular extends CheckBox {

    public CheckBoxSFTextRegular(Context context) {
        super(context);
        applyCustomFont();
    }

    public CheckBoxSFTextRegular(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont();
    }

    public CheckBoxSFTextRegular(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont();
    }

    private void applyCustomFont() {
        if (!TextUtils.isEmpty(Constants.SFTEXT_REGULAR)) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.SFTEXT_REGULAR);
            setTypeface(tf);
        }
    }
}
