package com.lexcorp.demoapp.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.lexcorp.demoapp.model.Messages;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FirebaseUtility {

    private static final String TAG = FirebaseUtility.class.getName();
    private Context mContext;
    private FirebaseFirestore db;
    private String chatId;
    private String userID;
    private MessageCommunicationListener listener;
    private ArrayList<Messages> messagesList;
    private FirebaseStorage storage;
    private ProgressDialog progress;

    public FirebaseUtility(Context mContext) {
        this.mContext = mContext;
        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        messagesList = new ArrayList<>();
    }

    public FirebaseUtility setChatId(String chatId) {
        this.chatId = chatId;
        return this;
    }

    public FirebaseUtility setUserID(String userID) {
        this.userID = userID;
        return this;
    }

    public void getAllMessages() {
        CollectionReference messageRef = db.collection(Constants.CHATS)
                .document(chatId)
                .collection(Constants.MESSAGES);

        messageRef.orderBy(Constants.DATE, Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e(TAG, "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            Messages item = dc.getDocument().toObject(Messages.class);
                            if (dc.getType() == DocumentChange.Type.ADDED) {
                                messagesList.add(item);
                            }
                        }
                        if (listener != null) {
                            listener.onGetAllMessages(messagesList);
                        }
                    }
                });
    }

    public void sendMessage(final String message, String type, String fileName) {
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(mContext, "empty message", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, Object> data = new HashMap<>();
        data.put(Constants.ID, userID);
        data.put(Constants.MESSAGE, message);
        data.put(Constants.FILE_NAME, fileName);
        data.put(Constants.DATE, new Date());
        data.put(Constants.TYPE, type);

        CollectionReference messageRef = db.collection(Constants.CHATS)
                .document(chatId)
                .collection(Constants.MESSAGES);

        messageRef.add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        if (Constants.TEXT.equals(type)) {
                            updateLastMessage(message);
                        } else {
                            updateLastMessage(type);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    private void updateLastMessage(String message) {
        if (TextUtils.isEmpty(chatId))
            return;

        Map<String, Object> data = new HashMap<>();
        data.put(Constants.LAST_MESSAGE, message);
        data.put(Constants.DATE, new Date());

        DocumentReference messageRef = db.collection(Constants.CHATS).document(chatId);

        messageRef.set(data, SetOptions.merge());
    }

    public void uploadFile(String filePath, String type) {
        showProgress();

        StorageReference storageRef = storage.getReference();

        Uri file = Uri.fromFile(new File(filePath));
        String storagePath;
        switch (type) {
            case Constants.IMAGE:
                storagePath = chatId + "/images/" + file.getLastPathSegment();
                break;
            case Constants.VIDEO:
                storagePath = chatId + "/video/" + file.getLastPathSegment();
                break;
            case Constants.DOC:
                storagePath = chatId + "/document/" + file.getLastPathSegment();
                break;
            default:
                storagePath = chatId + "/" + file.getLastPathSegment();
                break;
        }

        StorageReference fileRef = storageRef.child(storagePath);
        UploadTask uploadTask = fileRef.putFile(file);

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double percentage = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                progress.setProgress((int) percentage);
            }
        }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                return fileRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                hideProgress();
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Log.e("Download Uri", downloadUri.toString());
                    sendMessage(downloadUri.toString(), type, file.getLastPathSegment());
                } else {
                    Toast.makeText(mContext, "uploading fails", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showProgress() {
        progress = new ProgressDialog(mContext);
        progress.setMessage("Sending...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(false);
        progress.setProgress(0);
        progress.setMax(100);
        progress.show();
    }

    private void hideProgress() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
        }
    }

    public FirebaseUtility setCommunicationListener(MessageCommunicationListener listener) {
        this.listener = listener;
        return this;
    }

    public interface MessageCommunicationListener {
        public void onGetAllMessages(ArrayList<Messages> messagesList);
    }
}
