package com.lexcorp.demoapp.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static final String ENCODING_UTF_8 = "UTF-8";

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static SpannableStringBuilder getBoldString(Context context, String s, String[] fonts, int[] colorList, String[] words) {
        if (TextUtils.isEmpty(s))
            return null;

        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        try {
            for (int i = 0; i < words.length; i++) {
                if (s.contains(words[i])) {
                    if (colorList != null) {
                        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, colorList[i])), s.indexOf(words[i]),
                                s.indexOf(words[i]) + words[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                    if (fonts != null && fonts.length > i) {
                        Typeface font = Typeface.createFromAsset(context.getAssets(), fonts[i]);
                        ss.setSpan (new CustomTypefaceSpan("", font), s.indexOf(words[i]), s.indexOf(words[i]) + words[i].length(),
                                Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ss;
    }

    public static SpannableStringBuilder getColorString(Context context, String s, String word, int color) {
        SpannableStringBuilder ss = new SpannableStringBuilder(s);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), s.indexOf(word),
                s.indexOf(word) + word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    public static void makeLinks(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void makeLinks(CheckBox checkBox, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(checkBox.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = checkBox.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink,
                    startIndexOfLink + link.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        checkBox.setHighlightColor(Color.TRANSPARENT); // prevent TextView change background when highlight
        checkBox.setMovementMethod(LinkMovementMethod.getInstance());
        checkBox.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static String priceWith$(Object object) {
        String price = String.valueOf(object);
        if (TextUtils.isEmpty(price)) {
            return "$0";
        }
        if (price.contains("$")) {
            price = price.replace("$", "");
        }
        return "$" + price.trim();
    }

    public static String priceWithout$(Object object) {
        String price = String.valueOf(object);
        if (TextUtils.isEmpty(price)) {
            return "0";
        }
        if (price.contains("$")) {
            price = price.replace("$", "");
        }
        return price.trim();
    }

    public static void openSoftKeyboard(Activity activity, View view) {
        if (activity == null)
            return;

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static String numberFormat(String number, int decimalPoint) {
        try {
            Double d = Double.parseDouble(number);
            StringBuilder pattern = new StringBuilder();
            pattern.append("0.");
            for (int i = 0; i < decimalPoint; i++) {
                pattern.append("0");
            }
            NumberFormat nf = new DecimalFormat(pattern.toString());
            return nf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public static String numberFormat(double number, int decimalPoint) {
        try {
            StringBuilder pattern = new StringBuilder();
            pattern.append("0.");
            for (int i = 0; i < decimalPoint; i++) {
                pattern.append("0");
            }
            NumberFormat nf = new DecimalFormat(pattern.toString());
            return nf.format(number);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(number);
    }

    public static String changeDateFormat(String source, String target, String dateString) {
        SimpleDateFormat input = new SimpleDateFormat(source);
        SimpleDateFormat output = new SimpleDateFormat(target);
        try {
            Date date = input.parse(dateString);
            return output.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public static String changeDateFormat(Date date, String target) {
        SimpleDateFormat output = new SimpleDateFormat(target);
        return output.format(date);
    }

    public static void loadImage(Context context, String url, ImageView imageView) {
        if (context != null && imageView != null)
            Glide.with(context).load(url).into(imageView);
    }

    public static void loadImage(Context context, int drawable, ImageView imageView) {
        if (context != null && imageView != null)
            Glide.with(context).load(drawable).into(imageView);
    }

    public static String getFileExtFromBytes(File f) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            byte[] buf = new byte[5]; //max ext size + 1
            fis.read(buf, 0, buf.length);
            StringBuilder builder = new StringBuilder(buf.length);
            for (int i=1;i<buf.length && buf[i] != '\r' && buf[i] != '\n';i++) {
                builder.append((char)buf[i]);
            }
            return builder.toString().toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public static String getFileNameFromUrl(String url) {
        try {
            URL resource = new URL(url);
            String urlString = resource.getFile();
            return urlString.substring(urlString.lastIndexOf('/') + 1).split("\\?")[0].split("#")[0];
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }
}
