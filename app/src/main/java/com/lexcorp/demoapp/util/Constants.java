package com.lexcorp.demoapp.util;

public class Constants {

    //API API_V_2 PARAMS
    public static final String BASE_URL = "http://www.24task.com/";
    public static final String API_V_2 = "api_v2/";
    public static final String SERVICES = API_V_2 + "services/getServices.json";
    public static final String LANGUAGES = API_V_2 + "Languages/getLanguage.json";
    public static final String PAY_TYPE = API_V_2 + "PayTypes/getPayType.json";
    public static final String SKILLS = API_V_2 + "profileSkills/view/";
    public static final String LOGIN = API_V_2 + "profiles/login.json";
    public static final String SOCIAL_LOGIN = API_V_2 + "profiles/loginWith.json";
    public static final String SIGNUP = API_V_2 + "profiles/signup.json";
    public static final String UPDATE_PROFILE = API_V_2 + "profiles/update/";
    public static final String GET_PROFILE = API_V_2 + "profiles/view/";
    public static final String UPDATE_EXPERIENCE = API_V_2 + "experiences/add.json";
    public static final String ADD_PAY_TYPES = API_V_2 + "ProfilePayTypes/add.json";
    public static final String UPDATE_PAY_TYPES = API_V_2 + "profilePayTypes/update/";
    public static final String ADD_LANGUAGES = API_V_2 + "profileLanguages/add.json";
    public static final String ADD_SKILLS = API_V_2 + "profileSkills/skillAdd/";
    public static final String HEADLINES = API_V_2 + "headlines/add.json";
    public static final String SUMMARIES = API_V_2 + "summaries/add.json";
    public static final String EDUCATIONS = API_V_2 + "educations/add.json";
    public static final String LOCATION = API_V_2 + "Addresses/add.json";
    public static final String UPDATE_PASSWORD = API_V_2 + "profiles/updatePassword/";
    public static final String FEEDBACK = API_V_2 + "feedbacks/add.json";
    public static final String RESUME = API_V_2 + "resumes/add.json";
    public static final String FORGOT_PASSWORD = API_V_2 + "profiles/forgetPassword.json";
    public static final String RESET_PASSWORD = API_V_2 + "profiles/resetPassword.json";
    public static final String ADD_FREE_TRIAL = API_V_2 + "FreeTrials/add.json";
    public static final String TOKEN_EXPIRED = API_V_2 + "securities/isTokenExpired.json";

    public static final String API = "api/";
    //public static final String SERVICES = API + "services.json";
    public static final String PRICE_CALCULATION = API + "services/getPriceCalculation.json";
    public static final String PRIVACY = BASE_URL + "#/privacy-policy";
    public static final String TERMS_USE = BASE_URL + "#/terms-of-use";
    public static final String FAQS = BASE_URL + "#/faqs";
    public static final String ABOUTUS = BASE_URL + "#/about-us";

    public static final int AGENT_PROFILE = 1;
    public static final int CLIENT_PROFILE = 2;

    public static final int TAB_HOME = 0;
    public static final int TAB_CHAT = 1;
    public static final int TAB_FREE_TRIAL = 2;
    public static final int TAB_TASK_PRO = 2;
    public static final int TAB_CALCULATE = 3;
    public static final int TAB_CLIENT_PROFILE = 4;
    public static final int TAB_WORK_PROFILE = 3;

    public static final int DEVICE_TYPE = 2; // for android

    public static final String SFTEXT_REGULAR = "font/GoogleSans-Regular.ttf";
    public static final String SFTEXT_BOLD = "font/SanFranciscoText-Bold.otf";
    public static final String SFTEXT_MEDIUM = "font/GoogleSans-Medium.ttf";
    public static final String SFTEXT_SEMIBOLD = "font/SanFranciscoText-Semibold.otf";
    public static final String SFDISPLAY_BOLD = "font/SF-Pro-Display-Bold.otf";
    public static final String SFDISPLAY_REGULAR = "font/SF-Pro-Display-Regular.otf";
    public static final String HELVETICA_LIGHT = "font/HelveticaNeue Light.ttf";

    public static final String FCM_TOKEN = "fcm_token";
    public static final String JWT = "JWT";
    public static final String FROM_LOGIN = "from login";
    public static final String LOGIN_FINISH = "login_finish";
    public static final String USER_DATA = "user_data";
    public static final String PROFILE_DATA = "profile_data";
    public static final String IS_LOGIN = "isLogin";
    public static final String POLICY_URL = "policy_url";
    public static final String SCREEN_NAME = "screen_name";
    public static final String IS_EDIT = "isEdit";
    public static final String SKILL_DATA = "Skill Data";
    public static final String SERVICE_IDS = "Service ids";

    public static final String CHAT_DATA = "ChatData";
    public static final String CHATS = "chats";
    public static final String MESSAGES = "messages";
    public static final String FILE_NAME = "fileName";
    public static final String DATE = "date";
    public static final String LAST_MESSAGE = "last_message";
    public static final String MESSAGE = "message";
    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";
    public static final String DOC = "document";

    public static final String PREF_SERVICES = "services";
    public static final String SERVICE_NAME = "service_name";
    public static final String SERVICE_TYPE = "service_type";
    public static final String HIRE = "hire";
    public static final String WHY_US = "whyus";
    public static final String HOW_IT_WORKS = "howitworks";
    public static final String IS_CLIENT_ACCOUNT = "isClientAccount";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    // 0 - Basic
    // 1 - Conversational
    // 2 - Fluent
    // 3 - Native
    public static final int BASIC_ID = 0;
    public static final int CONVERSATIONAL_ID = 1;
    public static final int FLUENT_ID = 2;
    public static final int NATIVE_ID = 3;

    public static final String BASIC = "Basic";
    public static final String CONVERSATIONAL = "Conversational";
    public static final String FLUENT = "Fluent";
    public static final String NATIVE = "Native";

    // "Less than 1 year"
    // "1-3 years"
    // "4-6 years"
    // "7-9 years"
    // "10-12 years"
    // "13+ years"
    public static final int LESS_THAN_1_ID = 0;
    public static final int YEAR_1_3_ID = 1;
    public static final int YEAR_4_6_ID = 2;
    public static final int YEAR_7_9_ID = 3;
    public static final int YEAR_10_12_ID = 4;
    public static final int YEAR_13_ID = 5;

    public static final String LESS_THAN_1 = "Less than 1 year";
    public static final String YEAR_1_3 = "1-3 years";
    public static final String YEAR_4_6 = "4-6 years";
    public static final String YEAR_7_9 = "7-9 years";
    public static final String YEAR_10_12 = "10-12 years";
    public static final String YEAR_13 = "13+ years";

    //0 - associate, 1 - bachelor, 2 - master, 3 - doctorate
    public static final int Associate_ID = 0;
    public static final int Bachelor_ID = 1;
    public static final int Master_ID = 2;
    public static final int Doctorate_ID = 3;

    public static final String Associate = "Associate";
    public static final String Bachelor = "Bachelor";
    public static final String Master = "Master";
    public static final String Doctorate = "Doctorate";

    //0 - Beginner 1 - Intermediate 2 - Expert
    public static final int Beginner_ID = 0;
    public static final int Intermediate_ID = 1;
    public static final int Expert_ID = 2;

    public static final String Beginner = "Beginner";
    public static final String Intermediate = "Intermediate";
    public static final String Expert = "Expert";
}
