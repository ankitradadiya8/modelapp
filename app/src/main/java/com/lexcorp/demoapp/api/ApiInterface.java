package com.lexcorp.demoapp.api;

import com.lexcorp.demoapp.util.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Call<Object> login(@Field("username") String username, @Field("password") String password);
}
