package com.lexcorp.demoapp.adapter;

import android.content.Context;
import android.net.Uri;
import android.textview.TextViewSFTextRegular;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lexcorp.demoapp.R;
import com.lexcorp.demoapp.model.Messages;
import com.lexcorp.demoapp.ui.BaseActivity;
import com.lexcorp.demoapp.util.Constants;
import com.lexcorp.demoapp.util.Utils;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.SimpleViewHolder> {

    private Context context;
    private ArrayList<Messages> messagesList;
    private BaseActivity activity;

    public MessagesAdapter(Context context) {
        this.context = context;
        activity = (BaseActivity) context;
    }

    public void doRefresh(ArrayList<Messages> messagesList) {
        this.messagesList = messagesList;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_messages, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        Messages item = messagesList.get(position);

        if (item.id.equals(activity.getUserID())) { // Sender
            holder.frameOutgoing.setVisibility(View.VISIBLE);
            holder.frameIncoming.setVisibility(View.GONE);
            switch (item.type) {
                case Constants.TEXT:
                    holder.tvOutgoingMessage.setVisibility(View.VISIBLE);
                    holder.imgOutgoing.setVisibility(View.GONE);
                    holder.tvFileOutgoing.setVisibility(View.GONE);
                    holder.rlVideoSender.setVisibility(View.GONE);
                    holder.tvOutgoingMessage.setText(item.message);
                    break;
                case Constants.IMAGE:
                    holder.tvOutgoingMessage.setVisibility(View.GONE);
                    holder.imgOutgoing.setVisibility(View.VISIBLE);
                    holder.tvFileOutgoing.setVisibility(View.GONE);
                    holder.rlVideoSender.setVisibility(View.GONE);
                    Glide.with(context)
                            .load(item.message)
                            .apply(new RequestOptions().placeholder(R.color.lightgray))
                            .into(holder.imgOutgoing);
                    break;
                case Constants.VIDEO:
                    holder.tvOutgoingMessage.setVisibility(View.GONE);
                    holder.imgOutgoing.setVisibility(View.GONE);
                    holder.tvFileOutgoing.setVisibility(View.GONE);
                    holder.rlVideoSender.setVisibility(View.VISIBLE);
                    Glide.with(context)
                            .asBitmap()
                            .load(item.message)
                            .apply(new RequestOptions().placeholder(R.color.lightgray))
                            .into(holder.videoOutgoing);
                    break;
                case Constants.DOC:
                    holder.tvOutgoingMessage.setVisibility(View.GONE);
                    holder.imgOutgoing.setVisibility(View.GONE);
                    holder.tvFileOutgoing.setVisibility(View.VISIBLE);
                    holder.rlVideoSender.setVisibility(View.GONE);
                    holder.tvFileOutgoing.setText(item.fileName);
                    break;
                default:
                    holder.tvOutgoingMessage.setVisibility(View.GONE);
                    holder.imgOutgoing.setVisibility(View.GONE);
                    holder.tvFileOutgoing.setVisibility(View.GONE);
                    holder.rlVideoSender.setVisibility(View.GONE);
                    break;
            }
            holder.outgoingDate.setText(Utils.changeDateFormat(item.date, "hh:mm a"));
        } else {
            holder.frameOutgoing.setVisibility(View.GONE);
            holder.frameIncoming.setVisibility(View.VISIBLE);
            switch (item.type) {
                case Constants.TEXT:
                    holder.tvIncomingMessage.setVisibility(View.VISIBLE);
                    holder.imgIncoming.setVisibility(View.GONE);
                    holder.tvFileIncoming.setVisibility(View.GONE);
                    holder.rlVideoReceiver.setVisibility(View.GONE);
                    holder.tvIncomingMessage.setText(item.message);
                    break;
                case Constants.IMAGE:
                    holder.tvIncomingMessage.setVisibility(View.GONE);
                    holder.imgIncoming.setVisibility(View.VISIBLE);
                    holder.tvFileIncoming.setVisibility(View.GONE);
                    holder.rlVideoReceiver.setVisibility(View.GONE);
                    Glide.with(context)
                            .load(item.message)
                            .apply(new RequestOptions().placeholder(R.color.lightgray))
                            .into(holder.imgIncoming);
                    break;
                case Constants.VIDEO:
                    holder.tvIncomingMessage.setVisibility(View.GONE);
                    holder.imgIncoming.setVisibility(View.GONE);
                    holder.tvFileIncoming.setVisibility(View.GONE);
                    holder.rlVideoReceiver.setVisibility(View.VISIBLE);
                    Glide.with(context)
                            .asBitmap()
                            .load(item.message)
                            .apply(new RequestOptions().placeholder(R.color.lightgray))
                            .into(holder.videoIncoming);
                    break;
                case Constants.DOC:
                    holder.tvIncomingMessage.setVisibility(View.GONE);
                    holder.imgIncoming.setVisibility(View.GONE);
                    holder.tvFileIncoming.setVisibility(View.VISIBLE);
                    holder.rlVideoReceiver.setVisibility(View.GONE);
                    holder.tvFileIncoming.setText(item.fileName);
                    break;
                default:
                    holder.tvIncomingMessage.setVisibility(View.GONE);
                    holder.imgIncoming.setVisibility(View.GONE);
                    holder.tvFileIncoming.setVisibility(View.GONE);
                    holder.rlVideoReceiver.setVisibility(View.GONE);
                    break;
            }
            holder.incomingDate.setText(Utils.changeDateFormat(item.date, "hh:mm a"));
        }
    }

    @Override
    public int getItemCount() {
        return messagesList != null ? messagesList.size() : 0;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_outgoing_message)
        TextViewSFTextRegular tvOutgoingMessage;
        @BindView(R.id.img_outgoing)
        RoundedImageView imgOutgoing;
        @BindView(R.id.video_outgoing)
        RoundedImageView videoOutgoing;
        @BindView(R.id.rl_video_sender)
        RelativeLayout rlVideoSender;
        @BindView(R.id.tv_file_outgoing)
        TextViewSFTextRegular tvFileOutgoing;
        @BindView(R.id.rl_sender)
        RelativeLayout rlSender;
        @BindView(R.id.outgoing_date)
        TextViewSFTextRegular outgoingDate;
        @BindView(R.id.frame_outgoing)
        RelativeLayout frameOutgoing;
        @BindView(R.id.tv_incoming_message)
        TextViewSFTextRegular tvIncomingMessage;
        @BindView(R.id.img_incoming)
        RoundedImageView imgIncoming;
        @BindView(R.id.video_incoming)
        RoundedImageView videoIncoming;
        @BindView(R.id.rl_video_receiver)
        RelativeLayout rlVideoReceiver;
        @BindView(R.id.tv_file_incoming)
        TextViewSFTextRegular tvFileIncoming;
        @BindView(R.id.rl_receiver)
        RelativeLayout rlReceiver;
        @BindView(R.id.incoming_date)
        TextViewSFTextRegular incomingDate;
        @BindView(R.id.frame_incoming)
        RelativeLayout frameIncoming;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
