package com.lexcorp.demoapp.model;

import java.io.Serializable;
import java.util.Date;

public class Chat implements Serializable {

    public String sender_id;
    public String sender_name;
    public String sender_pic;
    public String receiver_id;
    public String receiver_name;
    public String receiver_pic;
    public Date date;
    public String last_message;
    public String id;
    public boolean isSOnline;
    public boolean isROnline;
    public boolean isSSeen;
    public boolean isRSeen;

    public Chat() {
    }
}
