package com.lexcorp.demoapp.ui.tabs;

import android.os.Bundle;

import com.lexcorp.demoapp.R;
import com.lexcorp.demoapp.ui.BaseActivity;

public class ProfileActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
