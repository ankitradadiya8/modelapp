package com.lexcorp.demoapp.ui;

import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextMedium;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.lexcorp.demoapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_email_phone)
    EditText etEmailPhone;
    @BindView(R.id.input_email_phone)
    TextInputLayout inputEmailPhone;
    @BindView(R.id.tv_toolbar_title)
    TextViewSFTextMedium tvToolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.login));

        etEmailPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inputEmailPhone.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void submitForm() {
        if (isValid())
            Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private String getEnteredText() {
        return etEmailPhone.getText().toString().trim();
    }

    private boolean isValid() {
        String text = getEnteredText();
        if (isEmpty(text)) {
            inputEmailPhone.setError(getString(R.string.invalid_mobile_email));
            requestFocus(etEmailPhone);
            return false;
        }

        if (isOnlyNumberContains(text) && isValidMobile(text)) {
            inputEmailPhone.setError(getString(R.string.invalid_mobile_email));
            requestFocus(etEmailPhone);
            return false;
        }

        if (!isOnlyNumberContains(text) && isValidEmail(text)) {
            inputEmailPhone.setError(getString(R.string.invalid_mobile_email));
            requestFocus(etEmailPhone);
            return false;
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @OnClick({R.id.img_back, R.id.ll_fb, R.id.ll_google, R.id.btn_next, R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.ll_fb:
                break;
            case R.id.ll_google:
                break;
            case R.id.btn_next:
                submitForm();
                break;
            case R.id.tv_register:
                redirectActivity(RegisterActivity.class);
                break;
        }
    }
}
