package com.lexcorp.demoapp.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.lexcorp.demoapp.R;
import com.lexcorp.demoapp.api.ApiClient;
import com.lexcorp.demoapp.api.ApiInterface;
import com.lexcorp.demoapp.model.GeneralModel;
import com.lexcorp.demoapp.util.Constants;
import com.lexcorp.demoapp.util.Preferences;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    private Dialog dialog;
    private static final String PASSWORD_PATTERN = "((?=.*[a-zA-Z])(?=.*[@#$%0-9]).{8,24})";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ApiInterface getService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    public void failureError(String message) {
        hideProgress();
        if (!isEmpty(message))
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void validationError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public boolean checkStatus(GeneralModel model) {
        if (model == null)
            return false;

        if (model.success != null) {
            switch (model.success) {
                case "1":
                    return true;
            }
        } else if (model.flag == 1) {
            return true;
        } else if (model.flag == 0) {
            if (model.msg != null && model.msg.equalsIgnoreCase("Expired Token.")) {
                Preferences.clearPreferences(this);
//                Preferences.saveUserData(this, null);
//                Preferences.setProfileData(this, null);
                goToLoginSignup(true, true);
                return false;
            }
        }
        failureError(model.msg);
        return false;
    }

    public boolean checkStatus(String response) {
        if (isEmpty(response))
            return false;

        try {
            JSONObject jsonObject = new JSONObject(response);
            String msg = null;
            if (jsonObject.has("msg")) {
                msg = jsonObject.getString("msg");
            }
            if (jsonObject.has("success")) {
                String success = jsonObject.getString("success");
                if (!isEmpty(success)) {
                    switch (success) {
                        case "0":
                            if (msg != null && !isEmpty(msg)) {
                                failureError(msg);
                            }
                            return false;
                        case "1":
                        case "1.0":
                            return true;
                    }
                }
            } else if (jsonObject.has("flag")) {
                String flag = jsonObject.getString("flag");
                if (flag.equals("1") || flag.equals("1.0")) {
                    return true;
                } else if (flag.equals("0") || flag.equals("0.0")) {
                    if (msg != null && msg.equalsIgnoreCase("Expired Token.")) {
                        Preferences.clearPreferences(this);
//                        Preferences.saveUserData(this, null);
//                        Preferences.setProfileData(this, null);
                        goToLoginSignup(true, true);
                        return false;
                    }
                }
            }
            failureError(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isClientAccount() {
        return Preferences.readBoolean(this, Constants.IS_CLIENT_ACCOUNT, false);
    }

    public int getProfileTypeId() {
        return isClientAccount() ? Constants.CLIENT_PROFILE : Constants.AGENT_PROFILE;
    }

    public boolean isLogin() {
        return Preferences.readBoolean(this, Constants.IS_LOGIN, false);
    }

    public String getToken() {
        return Preferences.readString(this, Constants.FCM_TOKEN, "");
    }

    public String getJWT() {
        return Preferences.readString(this, Constants.JWT, "");
    }

    public String getUserID() {
        return "12345";
    }

    public String getDeviceType() {
        return "1"; // for Android
    }

    public String getTimeZone() {
        return TimeZone.getDefault().getID(); // for Android
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }

    public void gotoMainActivity(int screen) {
        if (getParent() != null) {
            ((MainActivity) getParent()).gotoMainActivity(screen);
        } else {
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra(Constants.SCREEN_NAME, screen);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
            finishToRight();
        }
    }

    public void goToLoginSignup(boolean isLogin, boolean isNeedToFinish) {
//        Intent i = new Intent(this, LoginSignUpActivity.class);
//        i.putExtra(Constants.FROM_LOGIN, isLogin);
//        i.putExtra(Constants.LOGIN_FINISH, isNeedToFinish);
//        startActivity(i);
//        openToTop();
    }

    public void clearTopActivity(Class<?> activityClass) {
        if (getParent() != null) {
            ((MainActivity) getParent()).clearTopActivity(activityClass);
        } else {
            Intent i = new Intent(this, activityClass);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
            finishToRight();
        }
    }

    public void redirectTab(int tabIndex) {
        if (getParent() != null) {
            ((MainActivity) getParent()).getTabHost().setCurrentTab(tabIndex);
        }
    }

    public void redirectActivity(Class<?> activityClass) {
        if (getParent() != null) {
            ((MainActivity) getParent()).redirectActivity(activityClass);
        } else {
            startActivity(new Intent(this, activityClass));
            openToLeft();
        }
    }

    public void openToTop() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void openToLeft() {
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }

    public void finishToBottom() {
        overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
    }

    public void finishToRight() {
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void showProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.setCancelable(false);
        RotateLoading rotateLoading = dialog.findViewById(R.id.rotateloading);
        rotateLoading.start();
        dialog.show();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.0f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void hideProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public boolean isOnlyNumberContains(String text) {
        try {
            int num = Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isValidPassword(String target) {
        return (!isEmpty(target) && Pattern.compile(PASSWORD_PATTERN).matcher(target).matches());
    }

    public boolean isValidEmail(String target) {
        return (!isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public boolean isValidMobile(String phone) {
        if (!isEmpty(phone) && phone.contains("+"))
            phone = phone.replace("+", "");
        return (!isEmpty(phone) && Double.parseDouble(phone) > 0 && !Pattern.matches("[a-zA-Z]+", phone) && phone.length() == 10);
    }

    public boolean isValidUrl(String url) {
        return (!isEmpty(url) && Patterns.WEB_URL.matcher(url.toLowerCase()).matches());
    }

    public boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    public void showFeedbackDialog() {
//        final Dialog dialog = new Dialog(this, R.style.Theme_Design_Light_BottomSheetDialog);
//        dialog.setTitle(null);
//        dialog.setContentView(R.layout.dialog_feedback);
//        dialog.setCancelable(true);
//        TextView tvCancel = dialog.findViewById(R.id.tv_cancel);
//        TextView tvSend = dialog.findViewById(R.id.tv_send);
//        final EditText etFeedback = dialog.findViewById(R.id.et_feedback);
//        tvCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        tvSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!isEmpty(etFeedback.getText().toString().trim())) {
//                    sendFeedback(etFeedback.getText().toString());
//                    dialog.dismiss();
//                } else {
//                    Toast.makeText(BaseActivity.this, "Enter message", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.gravity = Gravity.BOTTOM;
//        dialog.show();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.getWindow().setAttributes(lp);
    }

    public void sendFeedback(String message) {
//        if (!isNetworkConnected())
//            return;
//
//        showProgress();
//
//        Call<GeneralModel> call = getService().addFeedback(getUserID(), message, getJWT());
//        call.enqueue(new Callback<GeneralModel>() {
//            @Override
//            public void onResponse(Call<GeneralModel> call, Response<GeneralModel> response) {
//                if (response.body() != null) {
//                    if (checkStatus(response.body())) {
//                        toastMessage(response.body().msg);
//                    }
//                }
//                hideProgress();
//            }
//
//            @Override
//            public void onFailure(Call<GeneralModel> call, Throwable t) {
//                failureError("add feedback failed");
//            }
//        });
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Share via..."));
    }
}
