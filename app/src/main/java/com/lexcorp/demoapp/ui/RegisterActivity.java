package com.lexcorp.demoapp.ui;

import android.edittext.EditTextSFTextRegular;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFTextMedium;
import android.textview.TextViewSFTextRegular;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.lexcorp.demoapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.tv_toolbar_title)
    TextViewSFTextMedium tvToolbarTitle;
    @BindView(R.id.et_mobile_no)
    EditTextSFTextRegular etMobileNo;
    @BindView(R.id.input_mobile)
    TextInputLayout inputMobile;
    @BindView(R.id.et_email)
    EditTextSFTextRegular etEmail;
    @BindView(R.id.input_email)
    TextInputLayout inputEmail;
    @BindView(R.id.et_password)
    EditTextSFTextRegular etPassword;
    @BindView(R.id.input_password)
    TextInputLayout inputPassword;
    @BindView(R.id.tv_tnc)
    TextViewSFTextRegular tvTnc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(getString(R.string.register));

        etMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inputMobile.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inputEmail.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                inputPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void submitForm() {
        if (!isValid()) {
            return;
        }

        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private String getEmail() {
        return etEmail.getText().toString().trim();
    }

    private String getMobileNo() {
        return etMobileNo.getText().toString().trim();
    }

    private String getPassword() {
        return etPassword.getText().toString().trim();
    }

    private boolean isValid() {
        if (!isValidMobile(getMobileNo())) {
            inputMobile.setError(getString(R.string.invalid_mobile_no));
            requestFocus(etMobileNo);
            return false;
        } else {
            inputMobile.setErrorEnabled(false);
        }

        if (!isValidEmail(getEmail())) {
            inputEmail.setError(getString(R.string.invalid_email_address));
            requestFocus(etEmail);
            return false;
        } else {
            inputEmail.setErrorEnabled(false);
        }

        if (!isValidPassword(getPassword())) {
            inputPassword.setError("Invalid Password");
            requestFocus(etPassword);
            return false;
        } else {
            inputPassword.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @OnClick({R.id.img_back, R.id.ll_fb, R.id.ll_google, R.id.btn_register, R.id.tv_tnc, R.id.tv_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.ll_fb:
                break;
            case R.id.ll_google:
                break;
            case R.id.btn_register:
                submitForm();
                break;
            case R.id.tv_tnc:
                break;
            case R.id.tv_login:
                finish();
                break;
        }
    }
}
