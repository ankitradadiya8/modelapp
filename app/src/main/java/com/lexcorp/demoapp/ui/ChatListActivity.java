package com.lexcorp.demoapp.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.lexcorp.demoapp.R;
import com.lexcorp.demoapp.adapter.RecyclerviewAdapter;
import com.lexcorp.demoapp.model.Chat;
import com.lexcorp.demoapp.util.Constants;
import com.lexcorp.demoapp.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListActivity extends BaseActivity implements RecyclerviewAdapter.OnViewBindListner {

    private static final String TAG = ChatListActivity.class.getName();

    @BindView(R.id.rv_chat_list)
    RecyclerView rvChatList;

    private FirebaseFirestore db;
    private ArrayList<Chat> chatLists;
    private RecyclerviewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvChatList.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvChatList.getContext(),
                linearLayoutManager.getOrientation());
        rvChatList.addItemDecoration(dividerItemDecoration);

        db = FirebaseFirestore.getInstance();

        chatLists = new ArrayList<>();

        getAllChatList();
    }

    private void getAllChatList() {
        if (!isNetworkConnected())
            return;

        showProgress();

        db.collection(Constants.CHATS).orderBy(Constants.DATE, Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        hideProgress();
                        if (e != null) {
                            Log.e(TAG, "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            Chat item = dc.getDocument().toObject(Chat.class);
                            if (dc.getType() == DocumentChange.Type.ADDED) {
                                chatLists.add(item);
                            } else if (dc.getType() == DocumentChange.Type.MODIFIED) {
                                Log.e("New Index", dc.getNewIndex() + " Old Index : " + dc.getOldIndex());
                                chatLists.remove(dc.getOldIndex());
                                chatLists.add(0, item);
                            }
                        }
                        setAdapter();
                    }
                });
    }

    private void setAdapter() {
        if (chatLists != null && chatLists.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new RecyclerviewAdapter(chatLists, R.layout.item_chat_list, this);
            }

            mAdapter.doRefresh(chatLists);

            if (rvChatList.getAdapter() == null) {
                rvChatList.setAdapter(mAdapter);
            }
        }
    }

    @Override
    public void bindView(View view, final int position) {
        TextView tvReceiverName = view.findViewById(R.id.tv_receiver_name);
        TextView tvLastMessage = view.findViewById(R.id.tv_last_message);
        TextView tvTime = view.findViewById(R.id.tv_time);
        ImageView imgPic = view.findViewById(R.id.img_receiver);

        final Chat item = chatLists.get(position);
        // Check userId for sender or receiver
        Glide.with(this).load(item.receiver_pic).into(imgPic);
        tvReceiverName.setText(item.receiver_name);
        tvLastMessage.setText(item.last_message);
        tvTime.setText(Utils.changeDateFormat(item.date, "hh:mm a"));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChatListActivity.this, ChatMessagesActivity.class);
                i.putExtra(Constants.CHAT_DATA, chatLists.get(position));
                startActivity(i);
            }
        });
    }
}
