package com.lexcorp.demoapp.ui;

import android.os.Bundle;
import android.view.View;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.firestore.core.FirestoreClient;
import com.lexcorp.demoapp.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class IntroScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_play, R.id.tv_register, R.id.tv_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_play:
                break;
            case R.id.tv_register:
                redirectActivity(RegisterActivity.class);
                break;
            case R.id.tv_login:
                redirectActivity(LoginActivity.class);
                break;
        }
    }
}
