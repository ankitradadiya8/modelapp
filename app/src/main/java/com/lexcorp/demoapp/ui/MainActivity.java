package com.lexcorp.demoapp.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.widget.TabHost;

import com.lexcorp.demoapp.R;
import com.lexcorp.demoapp.ui.tabs.CalculateActivity;
import com.lexcorp.demoapp.ui.tabs.ChatActivity;
import com.lexcorp.demoapp.ui.tabs.HomeActivity;
import com.lexcorp.demoapp.ui.tabs.PlusActivity;
import com.lexcorp.demoapp.ui.tabs.ProfileActivity;
import com.lexcorp.demoapp.util.Constants;
import com.lexcorp.demoapp.util.Preferences;

public class MainActivity extends TabActivity implements TabHost.OnTabChangeListener {

    private TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost = findViewById(android.R.id.tabhost);

        setTab("home", R.drawable.tab_home, HomeActivity.class);
        setTab("chat", R.drawable.tab_chat, ChatActivity.class);
        setTab("plus", R.drawable.tab_plus, PlusActivity.class);
        if (isClientAccount()) {
            setTab("calculate", R.drawable.tab_calculate, CalculateActivity.class);
        }
        setTab("profile", R.drawable.tab_profile, ProfileActivity.class);

        mTabHost.setOnTabChangedListener(this);

        if (getIntent() != null) {
            int screen = getIntent().getIntExtra(Constants.SCREEN_NAME, 0);
            mTabHost.setCurrentTab(screen);
        }
    }

    public void setTab(String tag, int drawable, Class<?> activityClass) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag)
                .setIndicator("", ContextCompat.getDrawable(this, drawable))
                .setContent(new Intent(this, activityClass));
        mTabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId) {

    }

    public boolean isClientAccount() {
        return Preferences.readBoolean(this, Constants.IS_CLIENT_ACCOUNT, false);
    }

    public void gotoMainActivity(int screen) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.SCREEN_NAME, screen);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void clearTopActivity(Class<?> activityClass) {
        Intent i = new Intent(this, activityClass);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void redirectActivity(Class<?> activityClass) {
        startActivity(new Intent(this, activityClass));
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
}
