package com.lexcorp.demoapp.ui;

import android.app.Dialog;
import android.edittext.EditTextSFDisplayRegular;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.textview.TextViewSFDisplayRegular;
import android.textview.TextViewSFTextMedium;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lexcorp.demoapp.R;
import com.lexcorp.demoapp.adapter.MessagesAdapter;
import com.lexcorp.demoapp.model.Chat;
import com.lexcorp.demoapp.model.Messages;
import com.lexcorp.demoapp.util.Constants;
import com.lexcorp.demoapp.util.FirebaseUtility;
import com.lexcorp.demoapp.util.FirebaseUtility.MessageCommunicationListener;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.vincent.filepicker.filter.entity.VideoFile;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatMessagesActivity extends ImagePickerActivity implements MessageCommunicationListener {

    private static final String TAG = ChatMessagesActivity.class.getName();

    @BindView(R.id.rv_messages)
    RecyclerView rvMessages;
    @BindView(R.id.et_message)
    EditTextSFDisplayRegular etMessage;
    @BindView(R.id.rl_message)
    RelativeLayout rlMessage;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_name)
    TextViewSFDisplayRegular tvName;
    @BindView(R.id.tv_online)
    TextViewSFDisplayRegular tvOnline;
    @BindView(R.id.tv_project_title)
    TextViewSFTextMedium tvProjectTitle;

    private Chat chats;
    private ArrayList<Messages> messagesList;
    private MessagesAdapter mAdapter;
    private FirebaseUtility firebaseUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messages);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            chats = (Chat) getIntent().getSerializableExtra(Constants.CHAT_DATA);
        }

        messagesList = new ArrayList<>();

        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmpty(getMessage())) {
                    imgSend.setAlpha(0.5f);
                    imgSend.setClickable(false);
                } else {
                    imgSend.setAlpha(1f);
                    imgSend.setClickable(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        firebaseUtility = new FirebaseUtility(this)
                .setChatId(chats.id)
                .setUserID(getUserID())
                .setCommunicationListener(this);

        getAllMessages();
    }

    private void getAllMessages() {
        showProgress();
        firebaseUtility.getAllMessages();
    }

    private void setAdapter() {
        if (messagesList != null && messagesList.size() > 0) {
            if (mAdapter == null) {
                mAdapter = new MessagesAdapter(this);
            }

            mAdapter.doRefresh(messagesList);

            if (rvMessages.getAdapter() == null) {
                rvMessages.setAdapter(mAdapter);
            }
            rvMessages.scrollToPosition(messagesList.size() - 1);
        }
    }

    @OnClick({R.id.img_back, R.id.img_send, R.id.img_setting, R.id.img_attach})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_send:
                firebaseUtility.sendMessage(getMessage(), Constants.TEXT, "");
                etMessage.setText("");
                break;
            case R.id.img_setting:
                showSettingDialog();
                break;
            case R.id.img_attach:
                selectFileDialog();
                break;
        }
    }

    private String getMessage() {
        return etMessage.getText().toString().trim();
    }

    public void showSettingDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle(null);
        dialog.setContentView(R.layout.dialog_chat_setting);
        dialog.setCancelable(true);
        TextView tvCancel = dialog.findViewById(R.id.btn_cancel);
        TextView tvViewProfile = dialog.findViewById(R.id.tv_view_profile);
        TextView tvMute = dialog.findViewById(R.id.tv_mute);
        TextView tvManage = dialog.findViewById(R.id.tv_manage_project);
        TextView tvReport = dialog.findViewById(R.id.tv_report_freelancer);

        tvViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.TOP;
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onImageSelect(ArrayList<ImageFile> imgPaths) {
        firebaseUtility.uploadFile(imgPaths.get(0).getPath(), Constants.IMAGE);
    }

    @Override
    public void onVideoSelect(ArrayList<VideoFile> videoPath) {
        firebaseUtility.uploadFile(videoPath.get(0).getPath(), Constants.VIDEO);
    }

    @Override
    public void onDocumentSelect(ArrayList<NormalFile> docPath) {
        firebaseUtility.uploadFile(docPath.get(0).getPath(), Constants.DOC);
    }

    @Override
    public void onGetAllMessages(ArrayList<Messages> messagesList) {
        this.messagesList = messagesList;
        hideProgress();
        setAdapter();
    }
}
